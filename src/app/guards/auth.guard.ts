import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
    private router: Router,
    private storage: Storage,  
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.getLoginValid().then(valid => {
        if (valid) {
          return this.getRoleValid(next.data.section,next.data.view).then(valid2 => {
              return valid2
          })
          // return true
        } else {
        // this.router.navigate(['/login-comercio'], { queryParams: { return: state.url.trim() }});  

        //Si no esta logeado y quiere acceder al login se lo permito 
        if (next.data.section == "login" && next.data.view == "login-comercio") {
          return true
        }
        if (next.data.section == "login" && next.data.view == "login-pin-empleado") {
          this.router.navigate(['/login-comercio']);
          return false
        }
        // //Si no esta logeado y quiero acceder a alguna seccion reestringida lo redireccion al login
        if (next.data.section == "cocina" || next.data.section == "bar" ) {
          this.router.navigate(['/login-comercio']);
        } else if(next.data.section == "admin"){
          this.router.navigate(['/login-admin']);
        }       
          return false
        }
      })
      // this.router.navigate(['/login-comercio']);
      // return false
      
  } 


  getLoginValid = async () => {
    let login = await this.storage.get('isLoggin');
    if (login) {
      return true
    } else {
      return false
    }
  } 
  getRoleValid = async (section,view) => {
    let nivel = await this.storage.get('idNivel');
    let loginPin = await this.storage.get('isLogginPin');
    let idNivelEmpleado = await this.storage.get('idNivelEmpleado');
    var valid=false;
    switch (section) {
      case "bar":
        if (nivel == 7 && idNivelEmpleado == 8 && loginPin) {
          valid = true;
        } else if(nivel == 9) {
          this.router.navigate(['/cocina']);
          valid = false;
        } else {
          this.router.navigate(['/login-pin-empleado']);
          valid = false;
        }
        break;
      case "cocina":
        if (nivel == 9) {
          valid = true;
        } else {
          this.router.navigate(['/login-comercio']);
          valid = false;
        }
        break;
        case "admin":
          if (nivel == 1 || nivel == 2 || nivel == 4 || nivel == 5) {
            valid = true;
          } else {
            valid = false;
          }
        break;
        case "login":
          // if (view == "login-comercio" || view == "login-pin-empleado") {
          if (view == "login-comercio") {
            //Si esta logeado como comercio chequeo si esta logeado como mozo(Lo verifico en la otra funcion por eso llega hasta aca.)
            //Verifico que sea el nivel adecuado para acceder
            if (nivel == 7 ) {
              //Si esta logeado el pin y el nivel es el 8 (Empleado lo redirecciono a las mesas, sino al pin)
              if (loginPin && idNivelEmpleado == 8) {
                this.router.navigate(['/mesas']);
              } else {
                this.router.navigate(['/login-pin-empleado']);
              }
              valid = false;
            }else if(nivel == 9) {
              this.router.navigate(['/cocina']);
              valid = false;
            } else {
              //Si no es nivel 7 lo redirecciono al login
              this.router.navigate(['/login-comercio']);
              valid = true;
            }            
          }else if(view == "login-pin-empleado"){
            if (loginPin && idNivelEmpleado == 8) {
              this.router.navigate(['/mesas']);
              valid = false;
            }else if(nivel == 9) {
              this.router.navigate(['/cocina']);
              valid = false;
            } else {
              valid = true;
            }
          } 
        break;
    
      default:
        valid = false;
        break;
    }
    return valid    
  } 
}
