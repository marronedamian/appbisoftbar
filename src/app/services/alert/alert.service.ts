import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})

export class AlertService {
  constructor(
    private toastController: ToastController
  ) { }
  
    async presentToast(message: any, color: string, position: any) {
      const toast = await this.toastController.create({
        message: message,
        duration: 1000,
        position: position,
        color: color,
      });
      toast.present();
    }
  
    async presentToastAddProduct(message: any, color: string, position: any) {
      const toast = await this.toastController.create({
        message: message,
        duration: 300,
        position: position,
        color: color,
      });
      toast.present();
    }
}