import { ViewChild, Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { DeliveryService } from 'src/app/services/delivery/delivery.service';
import { AlertService } from 'src/app/services/alert/alert.service';

@Component({
  selector: 'app-couriers',
  templateUrl: './couriers.page.html',
  styleUrls: ['./couriers.page.scss'],
})
export class CouriersPage implements OnInit {
  infoCourier: any[];
  isIndeterminate: boolean;
  masterCheck: boolean;
  checkBoxList: Array<any>;
  couriersSearch: any;
  statusError: boolean = true;
  statusErrorSearch: boolean = true;
  msgError: string;

  @ViewChild('searchCourierList', { static: false }) inputSearchFocus;

  ngOnInit() {
    setTimeout(() => {
      this.inputSearchFocus.setFocus();
    }, 500)
  }

  constructor(
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private deliveryService: DeliveryService,
    private alertService: AlertService,
  ) {
    //-- Get Data --//
    this.infoCourier = this.navParams.get('courier');

    //-- Load Couriers --//
    this.loadCouriers();
  }

  //-- Load Couriers --//
  loadCouriers() {
    this.deliveryService.couriers().then((result) => {
      let data: any = result;

      if (data.valid) {
        this.checkBoxList = data.datos;

        // this.checkBoxList.map(function (e) {
        //   if (e.idCadete == infoCourier.idCadete) {
        //     e.isChecked = true;
        //   } else {
        //     e.isChecked = false;
        //   }

        //   if (e.idCadete != 0 && e.idCadete != infoCourier.idCadete) {
        //     e.isDisabled = true;
        //   } else {
        //     e.isDisabled = false;
        //   }
        // });

        this.couriersSearch = this.checkBoxList;

        console.log(this.couriersSearch);
      } else {
        this.statusError = false;
        this.msgError = data.msg;
      }
    }, (err) => {
      this.statusError = false;
      this.msgError = 'Ocurrio un error al listar los pedidos';
    });
  }

  //-- Search Couriers List --//
  getCouriers(ev: any) {
    // Reset items back to all of the items
    this.couriersSearch = this.checkBoxList;

    // Set val to the value of the searchbar
    var val = ev.target.value;

    // If the value is an empty string don't filter the items
    if (val.length != 0) {
      this.couriersSearch = this.checkBoxList.filter((item) => {
        return (item.nroPedido.indexOf(val) > -1);
      })

      if (this.couriersSearch.length == 0) {
        this.statusErrorSearch = false;
        this.msgError = 'No se encontraron pedidos';
      } else {
        this.statusErrorSearch = true;
      }
    }
  }

  //-- Set orders --//
  setOrderCourier() {
    this.ordersService.setOrderCourier(this.checkBoxList, this.infoCourier['idCadete']).then((result) => {
      let data: any = result;

      if (data.valid) {
        this.hideModal();
        this.alertService.presentToast(data.msg, 'success', 'bottom');
      } else {
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      }
    }, (err) => {
      this.statusError = false;
      this.alertService.presentToast("Ha ocurrido un error al asignar los pedidos", 'danger', 'bottom');
    });
  }

  //-- Check Couriers --//
  checkEvent(item) {
    const totalItems = this.checkBoxList.length;
    let checked = 0;
    this.checkBoxList.map(obj => {
      if (!obj.isDisabled) {
        if (obj.isChecked) {
          checked++
        }
      }
    });

    if (checked > 0 && checked < totalItems) {
      //If even one item is checked but not all
      this.isIndeterminate = true;
    } else if (checked == totalItems) {
      //If all are checked
      this.isIndeterminate = false;

      //Add Class
      let itemBox = document.querySelector('.itemCourier' + item.idCadete);
      itemBox.classList.remove("itemOrderCourier");   //remove the class     
      itemBox.classList.add("itemOrderCourier");   //add the class                   
    } else {
      //If none is checked
      this.isIndeterminate = false;
    }

    if (item.isChecked) {
      //Add Class
      let itemBox = document.querySelector('.itemCourier' + item.idCadete);
      itemBox.classList.add("itemOrderCourier");   //add the class               
    } else {
      //Remove Class
      let itemBox = document.querySelector('.itemOrder' + item.idCadete);
      itemBox.classList.remove("itemOrderCourier");   //remove the class      
    }
  }

  //-- Hide Modal --//
  hideModal() {
    this.popoverCtrl.dismiss();
  }
}