import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { EnvService } from '../env/env.service';

declare var require: any

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
  ) { }

  usersOnline() {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_usuarios/get_usuarios_online',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      }
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice listado mesas. Cod Err #2000"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }  
}
