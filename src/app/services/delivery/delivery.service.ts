import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { EnvService } from '../env/env.service';

declare var require: any

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
  ) { }


  setCourier(data) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/set_cadete',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        nombre: data.value['name'],
        celular: data.value['tel'],
      } 
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al obtener el pedido. Cod Err #2001"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  couriers() {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/get_cadetes',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      }
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al obtener el pedido. Cod Err #2001"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }
}
