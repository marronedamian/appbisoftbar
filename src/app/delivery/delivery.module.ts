import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DeliveryPageRoutingModule } from './delivery-routing.module';
import { DeliveryPage } from './delivery.page';
import { TimeAgoComponent } from '../pipes/time-ago/time-ago.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliveryPageRoutingModule,
    TimeAgoComponent
  ],
  declarations: [DeliveryPage]
})
export class DeliveryPageModule {}
