import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Storage } from '@ionic/storage';
import { Events } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-login-pin-empleado',
  templateUrl: './login-pin-empleado.page.html',
  styleUrls: ['./login-pin-empleado.page.scss'],
})
export class LoginPinEmpleadoPage implements OnInit {
  @Input() pagetitle: String = "Enter Pin";
  pin:string = "";
  statusMenu: boolean = true;

  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private storage: Storage,
    public event: Events,
    private socket: Socket, 
  ) {
    //--//
  }

  //-- Show/Hide Menu --//
  showHideMenu(){
    let classNameMenu = document.querySelector('#menuUser').className;
    let classMenu = classNameMenu.substr(93,101);
    let classHideMenu = classNameMenu.substr(0,8);

    if(classMenu == 'showMenu'){
      this.event.publish('statusMenuHide');
      this.statusMenu = false;  
    }else if(classMenu == 'hideMenu'){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }else if (classHideMenu == 'hideMenu' && this.statusMenu ){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }
  }

  ngOnInit() {
  }

  //-- KeyUp event --//
  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    var key = (event.keyCode || event.which);

    //Verify keypress only numbers - Digit or Numpad
    if((key >= 96 && key <= 105) || (key >= 48 && key <= 57)){
      this.handleInput(event.key);
    }

    //Delete item keypress backspace or delete
    if(key == 8 || key == 46){
      this.pin = this.pin.slice(0, -1);
    }

    //Loggin keypress enter
    if(key==13){
      this.emitEvent();
    }    
  }    

  //-- Dismiss Login Modal --//
  dismissLogin() {
    this.modalController.dismiss();
  }  

  emitEvent() {
    this.storage.get('idGenComercio').then((val) => {
        if(val != null && val != undefined && val != 'undefined'){
          let idGenComercio = val; 

          this.authService.login_pin(this.pin, idGenComercio).then((result) => {
            let data:any = result;

            if(data.valid){
                this.alertService.presentToast('Usuario logeado con &eacute;xito', 'success', 'bottom');

                this.storage.set('isLogginPin', data.valid);        
                this.storage.set('idNivelEmpleado', data.datos[0].idNivel);        
                this.storage.set('nombreMozo', data.datos[0].nombreCompleto);        
                this.storage.set('apellidoMozo', data.datos[0].apellido);        
                this.storage.set('idUsuarioMozo', data.datos[0].idUsuario);    

                this.navCtrl.navigateRoot('/mesas');
                this.event.publish('loggedInPin');

                // Set waiter online
                this.socket.emit('set-user-online', { idUserWaiter: data.datos[0].idUsuario, nameWaiter: data.datos[0].nombreCompleto, tablesOpen: data.mesasAbiertas });
            }else{      
              this.handleInput('clear');
              this.alertService.presentToast(data.msg, 'danger', 'bottom');
            } 
          }, (err) => {
            this.handleInput('clear');
            this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
          });             
        }else{
          this.handleInput('clear');
          this.alertService.presentToast("Su sesión finalizó", 'danger', 'bottom');
          this.navCtrl.navigateRoot('/login-comercio');
        }
    }); 
  }

  handleInput(pin: string) {
    if (pin === "clear") {
      this.pin = "";
      return;
    }

    this.pin += pin;

    if (this.pin.length === 4) {
      this.emitEvent();
      return;
    }
  }  

}
