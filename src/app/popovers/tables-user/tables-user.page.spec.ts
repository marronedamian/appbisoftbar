import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TablesUserPage } from './tables-user.page';

describe('TablesUserPage', () => {
  let component: TablesUserPage;
  let fixture: ComponentFixture<TablesUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablesUserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TablesUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
