import { Component, OnInit, } from '@angular/core';
import { Events, MenuController, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-cocina',
  templateUrl: './cocina.page.html',
  styleUrls: ['./cocina.page.scss'],
})
export class CocinaPage implements OnInit {
  statusMenu: boolean = true;
  time = new Date();
  sound: boolean = false;
  ordersList: any[];
  statusError: boolean = false;
  msgError: string;

  constructor(
    public event: Events,
    private menuController: MenuController,
    private ordersService: OrdersService,
    private alertService: AlertService,
    public router: Router,
    private storage: Storage,
    public popoverCtrl: PopoverController,    
  ) {
    //--//
  }

  ngOnInit() {
    //-- Clock --//
    setInterval(() => {
      this.time = new Date();
    }, 1000);    

    //-- Load All Orders --//
    this.loadAllOrders();
  }

  //-- Load All Orders --//
  loadAllOrders() {
    this.ordersService.orders(4, null, 1).then((result) => {
      let data: any = result;

      if (data.valid) {
        this.statusError = false;
        this.alertService.presentToast('Pedidos listados', 'success', 'bottom');
        this.ordersList = data.pedidos;

        console.log(this.ordersList);
      } else {
        this.statusError = true;
        this.msgError = data.msg;
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      }
    }, (err) => {
      this.statusError = true;
      this.msgError = 'Ocurrio un error al listar los pedidos';
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });
  }  

  //-- Sound --//
  playAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/Popcorn.ogg";
    audio.load();
    audio.play();
  }  

  //-- Sound ON/OFF --//
  changeSound(){
    this.sound = !this.sound;

    if(this.sound){
      this.playAudio();
    }
  }

  //-- Show/Hide Menu --//
  showHideMenu(){
    let classNameMenu = document.querySelector('#menuUser').className;
    let classMenu = classNameMenu.substr(93,101);
    let classHideMenu = classNameMenu.substr(0,8);

    if(classMenu == 'showMenu'){
      this.event.publish('statusMenuHide');
      this.statusMenu = false;  
    }else if(classMenu == 'hideMenu'){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }else if (classHideMenu == 'hideMenu' && this.statusMenu ){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }
  }
}
