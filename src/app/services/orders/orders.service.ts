import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { EnvService } from '../env/env.service';

declare var require: any

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
  ) { }

  orders(idTab,idEstadoPedido,idEstadoPedidoDetalle) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/get_pedidos_tab',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        idTab: idTab,
        idEstadoPedido: idEstadoPedido,
        idEstadoPedidoDetalle: idEstadoPedidoDetalle,
      } 
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al obtener el pedido. Cod Err #2001"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  detail(idTab: Number,idMesa: String, idMozo: Number) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/abrir_pedido',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        idTab: idTab,
        idMozo: idMozo,
        idMesa: idMesa,
      } 
    }; 
 
    return new Promise((res,rej) => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice abrir pedido. Cod Err #2000"}');
          rej(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  reset() {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/reset_numeracion_pedidos',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      }
    }; 
 
    return new Promise((res,rej) => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice abrir pedido. Cod Err #2000"}');
          rej(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  setOrderCourier(arrayPedidos: Array<any>, idCadete: Number) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/asignar_pedido_cadete',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        arrayPedidos: JSON.stringify(arrayPedidos),
        idCadete: idCadete,
      } 
    }; 
 
    return new Promise((res,rej) => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice abrir pedido. Cod Err #2000"}');
          rej(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  updateStatusOrder(item: Array<any>, estado: Number) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/actualizar_pedido',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        idGenPedido: item['idGenPedido'],
        numeroTicket: item['numeroTicket'],
        idEstado: estado,
      } 
    }; 
 
    return new Promise((res,rej) => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice abrir pedido. Cod Err #2000"}');
          rej(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  getOrder(idGenPedido: String) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/get_pedido',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        idGenPedido: idGenPedido,
      } 
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al obtener el pedido. Cod Err #2001"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  setOrder(idGenPedido:any, datosPedido: Array<any>) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/set_pedido_new',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        idGenPedido: idGenPedido,
        datosPedido: datosPedido,
      } 
    }; 
 
    return new Promise((resolve,reject) => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al obtener el pedido. Cod Err #2001"}');
          reject(result);
        }else{
          let result = JSON.parse(body);
          resolve(result);
        };
      });      
    })
  }

  setPayOrder(dataOrder:any) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_pedidos/set_venta_pedido_new',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        idGenPedido: dataOrder['idGenPedido'],
        idCliente: dataOrder['idCliente'],
        tipoFac: dataOrder['tipoFac'],
        idMozo: dataOrder['idMozo'],
        idEstado: dataOrder['idEstado'],
        idTab: dataOrder['idTab'],
      } 
    }; 
 
    return new Promise((resolve,reject) => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al obtener el pedido. Cod Err #2001"}');
          reject(result);
        }else{
          let result = JSON.parse(body);
          resolve(result);
        };
      });      
    })
  }
}
