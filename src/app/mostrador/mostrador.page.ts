import { Component, OnInit } from '@angular/core';
import { Events, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Router, NavigationExtras } from '@angular/router';
import { TouchSequence } from 'selenium-webdriver';
 
@Component({
  selector: 'app-mostrador',
  templateUrl: './mostrador.page.html',
  styleUrls: ['./mostrador.page.scss'],
})
export class MostradorPage implements OnInit {
  nameWaiter: string;
  statusMenu: boolean = true;
  statusMenuTables: boolean = false;
  idUserWaiter: Number = 0;
  totalOrders: Number = 0;
  ordersPending: Number = 0;
  orders: any[];
  ordersSearch: any[];  
  statusSearchError: boolean = false;
  statusError: boolean = false;
  msgError: string;

  constructor(
    public event: Events,
    private menuController: MenuController,
    private ordersService: OrdersService,
    private alertService: AlertService,
    public router: Router,
    private storage: Storage,
  ) {
    //--//
  }

  ngOnInit() {
    //-- Info Waiter --//
    this.getInfoWaiter();    
  }  

  //-- Get info Waiter --//
  getInfoWaiter(){
    this.storage.get('idUsuarioMozo').then((val) => {
      //-- List orders --//
      if(val){
        this.statusError = false;
        this.idUserWaiter = val;
        this.loadOrders();
        this.loadOrdersReady();

        this.storage.get('nombreMozo').then((name) => {
          if(name){
            this.nameWaiter = name;
          }
        })
      }else{
        this.statusError = true;
        this.msgError = 'Error al obtener los datos del mozo';
        this.alertService.presentToast('Error al obtener los datos del mozo', 'danger', 'bottom');
      }
    })
  }

  //-- Load Orders --//
  loadOrders(){
    this.ordersService.orders(2,null,1).then((result) => {
      let data:any = result;

      if(data.valid){
        this.statusError = false;
        this.alertService.presentToast('Pedidos listados', 'success', 'bottom');
        this.ordersPending = data.pedidos.length;
        
        let numOrders;
        if(data.reset_ult_pedido == 0){
          numOrders = Number(data.ult_pedido) + 1
        }else if(data.reset_ult_pedido == 1){
          numOrders = 1
        }
        
        this.totalOrders = numOrders;
        this.orders = data.pedidos;
        this.ordersSearch = data.pedidos;
      }else{      
        this.statusError = true;
        this.msgError = data.msg;
        this.totalOrders = Number(data.ult_pedido) + 1;
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.statusError = true;
      this.msgError = 'Ocurrio un error al listar los pedidos';
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });      
  }

  //-- Load Orders Ready --//
  loadOrdersReady(){
    this.ordersService.orders(2,null,7).then((result) => {
      let data:any = result;

      if(data.valid){
        //-- Return orders ready form counter --//
        this.event.publish('loadOrdersReady', data.pedidos); 
      }else{      
        //-- Return orders ready form counter --//
        this.event.publish('loadOrdersReady', null); 
      } 
    }, (err) => {
      this.statusError = true;
      this.alertService.presentToast("Ocurrio un error al listar los pedidos listos", 'danger', 'bottom');
    });      
  }
 
  //-- Reset Orders --//
  resetOrders(){
    this.ordersService.reset().then((result) => {
      let data:any = result;
      
      if(data.valid){
          this.alertService.presentToast(data.msg, 'success', 'bottom');
          this.totalOrders = 1;
          this.ordersPending = 0;
      }else{      
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    }); 
  }

  //-- Create order --//
  createOrder(){
    this.ordersService.detail(2,null,this.idUserWaiter).then((result) => {
      let data:any = result;
      
      if(data.valid){
          this.alertService.presentToast(data.msg, 'primary', 'bottom');
          this.router.navigate(['/detalle-pedido', data.idGenPedido]);
      }else{      
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    }); 
  }

  //-- Search orders --//
  getItems(ev: any) {
    // Reset items back to all of the items
    this.ordersSearch = this.orders;

    // Set val to the value of the searchbar
    var val = ev.target.value;

    // If the value is an empty string don't filter the items
    if (val.length != 0){
      this.ordersSearch = this.ordersSearch.filter((item) => {
        return (item.idPedido.indexOf(val.toLowerCase()) > -1);
      })    

      if(this.ordersSearch.length == 0){
        this.statusSearchError = true;
      }else{
        this.statusSearchError = false;
      }
    }
  }  

  //-- Show/Hide Menu --//
  showHideMenu(){
    let classNameMenu = document.querySelector('#menuUser').className;
    let classMenu = classNameMenu.substr(93,101);
    let classHideMenu = classNameMenu.substr(0,8);

    if(classMenu == 'showMenu'){
      this.event.publish('statusMenuHide');
      this.statusMenu = false;  
    }else if(classMenu == 'hideMenu'){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }else if (classHideMenu == 'hideMenu' && this.statusMenu ){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }
  }

  //-- Show/Hide Menu Tables --//
  showHideMenuTables(){
    // CSS
    let classNameMenu = document.querySelector('#menuInfoOrders').className;
    let classMenu = classNameMenu.substr(69,14);
    let classShowMenu = classNameMenu.substr(78,14);
    let classHideMenu = classNameMenu.substr(91,14);

    if(classMenu == 'showMenuTables' || classShowMenu == 'showMenuTables' || classHideMenu == 'showMenuTables'){
      this.event.publish('statusMenuTablesHide');
      this.statusMenuTables = false;
    }else if(classHideMenu == 'hideMenuTables' || classShowMenu == 'hideMenuTables'){
      this.event.publish('statusMenuTablesShow');
      this.statusMenuTables = true;
    }else if (classHideMenu == 'hideMenuTables' && this.statusMenuTables ){
      this.event.publish('statusMenuTablesShow');
      this.statusMenuTables = true;
    }

    // Mobile
    this.menuController.open('second').then((resultOpen)=>{
      if(!resultOpen){
        this.menuController.close('second');
      }else{
        this.menuController.open('second');
      }
    })    
  }    
}
