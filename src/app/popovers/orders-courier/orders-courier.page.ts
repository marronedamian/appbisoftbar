import { ViewChild, Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { AlertService } from 'src/app/services/alert/alert.service';

@Component({
  selector: 'app-orders-courier',
  templateUrl: './orders-courier.page.html',
  styleUrls: ['./orders-courier.page.scss'],
})
export class OrdersCourierPage implements OnInit {
  infoCourier: any[];
  isIndeterminate:boolean;
  masterCheck:boolean;
  checkBoxList: Array<any>;
  ordersCourierSearch:any;
  statusError: boolean = true;
  statusErrorSearch: boolean = true;
  msgError: string;

  @ViewChild('searchOrderList', {static: false}) inputSearchFocus;

  ngOnInit() {
    setTimeout(() => {
      this.inputSearchFocus.setFocus();
    },500)     
  }

  constructor(
    public navParams:NavParams,
    public popoverCtrl: PopoverController,    
    private ordersService: OrdersService,
    private alertService: AlertService,
  ){    
    //-- Get Data --//
    this.infoCourier = this.navParams.get('courier');

    //-- Load Orders --//
    this.loadOrders(this.infoCourier);
  }

  //-- Load Orders --//
  loadOrders(infoCourier){
    this.ordersService.orders(3,null,7).then((result) => {
      let data:any = result;

      if(data.valid){      
        this.checkBoxList = data.pedidos;

        this.checkBoxList.map(function(e){
          if(e.idCadete == infoCourier.idCadete){
            e.isChecked = true;
          }else{
            e.isChecked = false;
          }

          if(e.idCadete != 0 && e.idCadete != infoCourier.idCadete){
            e.isDisabled = true;
          }else{
            e.isDisabled = false;
          }          
        });

        this.ordersCourierSearch = this.checkBoxList;

        console.log(this.ordersCourierSearch);
      }else{      
        this.statusError = false;
        this.msgError = data.msg;
      } 
    }, (err) => {
      this.statusError = false;
      this.msgError = 'Ocurrio un error al listar los pedidos';
    });      
  }  
 
  //-- Search Orders List --//
  getItemsOrders(ev: any) {
    // Reset items back to all of the items
    this.ordersCourierSearch = this.checkBoxList;

    // Set val to the value of the searchbar
    var val = ev.target.value;

    // If the value is an empty string don't filter the items
    if (val.length != 0){
      this.ordersCourierSearch = this.checkBoxList.filter((item) => {
        return (item.nroPedido.indexOf(val) > -1);
      })    

      if(this.ordersCourierSearch.length == 0){
        this.statusErrorSearch = false;
        this.msgError = 'No se encontraron pedidos';
      }else{
        this.statusErrorSearch = true;
      }
    }
  }  

  //-- Set orders --//
  setOrderCourier(){
    this.ordersService.setOrderCourier(this.checkBoxList,this.infoCourier['idCadete']).then((result) => {
      let data:any = result;

      if(data.valid){      
        this.hideModal(); 
        this.alertService.presentToast(data.msg, 'success', 'bottom');
      }else{      
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.statusError = false;
      this.alertService.presentToast("Ha ocurrido un error al asignar los pedidos", 'danger', 'bottom');
    });  
  }

  //-- Check Orders --//
  checkMaster() {
    setTimeout(()=>{
      this.checkBoxList.forEach(obj => {
        if(!obj.isDisabled){
          obj.isChecked = this.masterCheck;
        }
      });
    });
  }
 
  checkEvent(item) {
    const totalItems = this.checkBoxList.length;
    let checked = 0;
    this.checkBoxList.map(obj => {
      if(!obj.isDisabled){
        if (obj.isChecked){ 
          checked++
        }
      }
    });

    if (checked > 0 && checked < totalItems) {
      //If even one item is checked but not all
      this.isIndeterminate = true;
      this.masterCheck = false;  
    } else if (checked == totalItems) {
      //If all are checked
      this.masterCheck = true;
      this.isIndeterminate = false;

      //Add Class
      let itemBox = document.querySelector('.itemOrder'+item.idDetallePedido);
      itemBox.classList.remove("itemOrderCourier");   //remove the class     
      itemBox.classList.add("itemOrderCourier");   //add the class            
      
      let itemTitleBox = document.querySelector('.itemTitleOrder');
      itemTitleBox.classList.remove("itemOrderCourier");   //remove the class     
      itemTitleBox.classList.add("itemOrderCourier");   //add the class            
    } else {
      //If none is checked
      this.isIndeterminate = false;
      this.masterCheck = false;

      //Remove Class
      let itemTitleBox = document.querySelector('.itemTitleOrder');
      itemTitleBox.classList.remove("itemOrderCourier");   //remove the class      
    }

    if (item.isChecked){ 
      //Add Class
      let itemBox = document.querySelector('.itemOrder'+item.idDetallePedido);
      itemBox.classList.add("itemOrderCourier");   //add the class               
    }else{
      //Remove Class
      let itemBox = document.querySelector('.itemOrder'+item.idDetallePedido);
      itemBox.classList.remove("itemOrderCourier");   //remove the class      
    }
  }

  //-- Hide Modal --//
  hideModal(){
    this.popoverCtrl.dismiss();
  }  
}
