import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersCourierPage } from './orders-courier.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersCourierPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersCourierPageRoutingModule {}
