import { NavParams, PopoverController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tables-user',
  templateUrl: './tables-user.page.html',
  styleUrls: ['./tables-user.page.scss'],
})
export class TablesUserPage implements OnInit {
  infoWaiter: any[];
  tablesWaiter: any[];

  constructor(
    public navParams:NavParams,
    public popoverCtrl: PopoverController,
  ) {
    //-- Get Data --//
    this.infoWaiter = this.navParams.get('waiter');
    this.tablesWaiter = this.navParams.get('tables');
  }

  ngOnInit() {
  }

  //-- Hide Modal --//
  hideModal(){
    this.popoverCtrl.dismiss();
  }

}
