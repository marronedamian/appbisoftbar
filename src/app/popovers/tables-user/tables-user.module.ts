import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TablesUserPageRoutingModule } from './tables-user-routing.module';

import { TablesUserPage } from './tables-user.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TablesUserPageRoutingModule
  ],
  declarations: [TablesUserPage]
})
export class TablesUserPageModule {}
