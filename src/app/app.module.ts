import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }    from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { HeaderComponent } from '../app/template/header/header.component';
import { TablesUserPageModule } from './popovers/tables-user/tables-user.module';
import { OrdersCourierPageModule } from './popovers/orders-courier/orders-courier.module';
import { CouriersPageModule } from './popovers/couriers/couriers.module';
 
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://tsoftpc.ddns.net:3001', options: {} };

@NgModule({
  declarations: [AppComponent,HeaderComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,    
    IonicModule.forRoot({
      backButtonText: '',
      // iconMode: 'md',
      mode: 'md',
      // pageTransition: 'md-transition'    
    }),
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(config),
    AppRoutingModule,
    HttpClientModule,
    TablesUserPageModule,
    OrdersCourierPageModule,
    CouriersPageModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
