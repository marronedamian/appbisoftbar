import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginPinEmpleadoPage } from './login-pin-empleado.page';

const routes: Routes = [
  {
    path: '',
    component: LoginPinEmpleadoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginPinEmpleadoPageRoutingModule {}
