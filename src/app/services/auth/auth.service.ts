import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { EnvService } from '../env/env.service';

declare var require: any

@Injectable({
  providedIn: 'root'
})

export class AuthService {
 
  constructor(
    private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
  ) { }
 
  login(user: String, password: String) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_login/login_comercio',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        user: user, 
        password: password
      } 
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice login comercio. Cod Err #2000"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  login_pin(pin: String, idGenComercio: String) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_login/login_empleado',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        pin: pin, 
        idGenComercio: idGenComercio
      } 
    }; 
 
    return new Promise((res,rej) => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice login empleado. Cod Err #2000"}');
          rej(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }
  
  login_pin_cocina(pin: String) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_login/login_cocina',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        pin: pin
      } 
    }; 
 
    return new Promise((res,rej) => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice login empleado. Cod Err #2000"}');
          rej(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }

  logout_waiter(idUsuario: String) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_login/logout_empleado',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        idUsuario: idUsuario
      } 
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice login empleado. Cod Err #2000"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }
}
