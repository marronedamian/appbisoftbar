import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPinEmpleadoPageRoutingModule } from './login-pin-empleado-routing.module';

import { LoginPinEmpleadoPage } from './login-pin-empleado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPinEmpleadoPageRoutingModule
  ],
  declarations: [LoginPinEmpleadoPage]
})
export class LoginPinEmpleadoPageModule {}
