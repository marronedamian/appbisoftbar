import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TablesUserPage } from './tables-user.page';

const routes: Routes = [
  {
    path: '',
    component: TablesUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TablesUserPageRoutingModule {}
