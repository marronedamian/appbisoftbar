import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrdersCourierPage } from './orders-courier.page';

describe('OrdersCourierPage', () => {
  let component: OrdersCourierPage;
  let fixture: ComponentFixture<OrdersCourierPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersCourierPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrdersCourierPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
