import { Platform, PopoverController, ModalController, NavController, MenuController, Events } from '@ionic/angular';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Component, ViewChild, HostListener } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertService } from 'src/app/services/alert/alert.service';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { DeliveryService } from 'src/app/services/delivery/delivery.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Socket } from 'ngx-socket-io';
// import { runInThisContext } from 'vm';
// import { exists } from 'fs';
import { OrdersCourierPage } from '../app/popovers/orders-courier/orders-courier.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChild('nameCourier', {static: false}) inputNameCourierFocus;

  isLoggin: boolean;
  isLogginPin: boolean;
  isLogginKitchen: boolean;
  pageMenuTables: boolean = false;
  pageMenuDetail: boolean = false;
  pageMenuDelivery: boolean = false;
  currentPage: string;
  stateKeypress: Array<number> = [];
  statusMenu: string = 'hideMenu';
  statusMenuTables: string = 'showMenuTables';
  statusMenuDetail: string = 'showMenuDetail';
  statusMenuDelivery: string = 'showMenuDelivery';
  userName: string;
  idUserWaiter: Number = 0;
  url: string;
  detailForm: FormGroup;
  table: any[];
  nameTable: string = '';
  categories: any[];
  items: any[];
  itemsTop: any[];
  idGenPedido: any;
  ticketValid: boolean = false;  
  ordersReady: Array<any> = [];
  subTotalOrder: Number = 0;
  idTab: number;
  totalTablesAvaible: Number = 0;
  checkNewCourier: boolean = false;
  couriers: Array<any> = [];
  totalCouriers: Number = 0;
  detailFormCourier: FormGroup;
  subTotal: number = 0;
  costoEnvio: number = 0;
  descuento: number = 0;
  total: number = 0;
  noActivePay:boolean = true;

  public appPages = [
    // {
    //   title: 'Home',
    //   url: '/home',
    //   icon: 'home',
    //   shortcut: 'H'
    // }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,  
    private navCtrl: NavController,
    public event: Events,
    private menuController: MenuController,
    private formBuilder: FormBuilder,
    public popoverCtrl: PopoverController,
    private router: Router,
    private alertService: AlertService,
    private ordersService: OrdersService,
    private deliveryService: DeliveryService,
    private authService: AuthService,
    private socket: Socket, 
  ) {
    //-- Init --//
    this.initializeApp();

    //-- Get current url --//
    this.routeEvent(this.router);

    //-- Login - Commerce --//
    event.subscribe('loggedIn', () => {
      this.isLoggin = true;
    });    

    this.storage.get('isLoggin').then((val) => {
      if(val == null || val == undefined || val == 'undefined'){
        this.isLoggin = false;      
      }else{
        this.isLoggin = val;      
      }
    });    

    //-- Login - Kitchen --//
    event.subscribe('loggedInKitchen', () => {
      this.isLogginKitchen = true;
    });    

    this.storage.get('isLogginKitchen').then((val) => {
      if(val == null || val == undefined || val == 'undefined'){
        this.isLogginKitchen = false;      
      }else{
        this.isLogginKitchen = val;      
      }
    });    

    //-- Login - Pin Employee --//
    event.subscribe('loggedInPin', () => {
      this.isLogginPin = true;

      //-- Verify Loggin commerce and login employeed --//
      this.verifyLoggin();
    });     

    this.storage.get('isLogginPin').then((val) => {
      if(val == null || val == undefined || val == 'undefined'){
        this.isLogginPin = false;      
      }else{
        this.isLogginPin = val;      

        // Load info waiter
        this.storage.get('nombreMozo').then((resultNameWaiter) => {
          if(resultNameWaiter){   
            this.userName = resultNameWaiter;

            this.storage.get('idUsuarioMozo').then((resultIdUserWaiter) => {
              if(resultIdUserWaiter){   
                this.idUserWaiter = resultIdUserWaiter;

                // Load scripts
                this.loadCouriers();
              }
            })                
          }
        })    
      }
    });        

    //-- Status - Menu --//
    event.subscribe('statusMenuHide', () => {
      this.statusMenu = 'hideMenu';
    });       

    event.subscribe('statusMenuShow', () => {
      this.statusMenu = 'showMenu';
    });       

    //-- Status - Menu Tables --//
    event.subscribe('statusMenuTablesHide', () => {
      this.statusMenuTables = 'hideMenuTables';
    });       

    event.subscribe('statusMenuTablesShow', () => {
      this.statusMenuTables = 'showMenuTables';
    });       

    //-- Status - Menu Delivery --//
    event.subscribe('statusMenuDeliveryHide', () => {
      this.statusMenuDelivery = 'hideMenuDelivery';
    });       

    event.subscribe('statusMenuDeliveryShow', () => {
      this.statusMenuDelivery = 'showMenuDelivery';
    });       

    //-- Status - Menu Detail --//
    event.subscribe('statusMenuDetailHide', () => {
      this.statusMenuDetail = 'hideMenuDetail';
    });       

    event.subscribe('statusMenuDetailShow', () => {
      this.statusMenuDetail = 'showMenuDetail';
    });       
 
    //-- Generate form datail order --//
    this.detailForm = this.formBuilder.group({
      products: this.formBuilder.array([
        this.formBuilder.group(
          {
            producto: [''],
            idGenProducto: [''],
            idTipoProducto: [''],
            cantidad:[''],
            precio:[''],
            subtotal:[''],
            openItem:[''],
            openComment:[''],
            comment:[''],
          }
        )
      ]),
    }); 
  
    //-- Generate form new courier --//
    this.detailFormCourier = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      tel: new FormControl('',),
    }); 

    //-- Return total tables avaible --//
    event.subscribe('totalTablesAvaible', (totalTablesAvaible) => {
      this.totalTablesAvaible = totalTablesAvaible;
    });     

    //-- Return orders ready form tables and counter --//
    event.subscribe('loadOrdersReady', (ordersReady) => {
      this.ordersReady = [];

      if(ordersReady!=null){
        this.ordersReady = ordersReady;
      }
    });     
     
    //-- Return detail order and categories --//
    event.subscribe('loadDetailOrder', (idGenPedido) => {
      this.idGenPedido = idGenPedido;
      this.subTotal = 0;
      this.costoEnvio = 0;
      this.descuento = 0;
      this.total = 0;    

      this.ordersService.getOrder(idGenPedido).then((result) => {
        let data:any = result;

        // Set name table
        this.table = data.mesa[0];
        this.nameTable = data.mesa[0].descripcion;
        this.idTab = data.mesa[0].idTab;

        // Set array categories and products
        this.categories = data.categorias;
        this.itemsTop = data.productos_destacados;
        this.items = data.productos;

        this.event.publish('setCategoriesProducts', this.nameTable, this.categories, this.itemsTop, this.items);         

        // Clean form
        const arr = <FormArray>this.detailForm.controls['products'];
        arr.controls = [];     

        // Formarray detail order
        const control = <FormArray>this.detailForm.controls['products'];

        // Delete first row
        control.removeAt(0);    

        // Show status response
        if(data.valid){
          // Show message
          this.alertService.presentToast(data.msg, 'success', 'bottom');

          if(data.datos.length > 0){
            // Valid that contains at least one element to enable the ticket
            this.ticketValid = true;
  
            // Complete detail order
            data.datos.forEach(element => {
              let subTotalItem =  parseInt(element.cantidad) * element.precio;
              control.push(this.formBuilder.group(
                  {
                    producto: [element.nombre],
                    idGenProducto: [element.idGenProducto],
                    idTipoProducto: [element.idTipoProducto],
                    cantidad:[parseInt(element.cantidad)],
                    precio:[element.precio],
                    subtotal:[subTotalItem],
                    openItem:[false],
                    openComment:[false],
                    comment:[""],
                  }
              ));    
              this.subTotal += subTotalItem;
              this.total +=  subTotalItem;
            });
          }
        }else{      
          // Show message
          this.alertService.presentToast(data.msg, 'danger', 'bottom');
        } 
      }, (err) => {
        // Show message
        this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
      }); 
    });       

    //-- Add product in order list --//
    event.subscribe('addItemDetailOrder', (item,idTipoProducto) => {
      const control = <FormArray>this.detailForm.controls['products'];
      let exist=control.value.find(value=> value.idGenProducto == item.idGenProducto);
      if(exist === undefined){
        control.push(this.formBuilder.group(
            {
              producto: [item.nombre],
              idGenProducto: [item.idGenProducto],
              idTipoProducto: [idTipoProducto],
              cantidad:[1],
              precio:[item.precioVenta],
              subtotal:[item.precioVenta],
              openItem:[false],
              openComment:[false],
              comment:[''],
            }
        ));  
        
      }else if(exist){
        control.value.find(value => {
          if(value.idGenProducto == item.idGenProducto){
            value.cantidad= value.cantidad+1;
            value.comment= "";
            value.subtotal= value.cantidad*value.precio;
            this.detailForm.controls['products'].setValue(control.value);
          }
        });
      }
      //I add the price of th item to the subtotal
      this.subTotal += Number(item.precioVenta); 
      this.total +=  Number(item.precioVenta);

      // Enable and disable btn order
      if (control.value.length > 0) {
        this.ticketValid = true;
      } else {
        this.ticketValid = false;
      }

      // Show message
      this.alertService.presentToastAddProduct("Producto agregado", 'success', 'top');    
    });
  }

  //-- Get current url --//
  routeEvent(router: Router){
    router.events.subscribe(e => {
      if(e instanceof NavigationEnd){
        this.currentPage = e.url;

        if(this.currentPage.slice(1) == 'mesas' || this.currentPage.slice(1) == 'mostrador'){
          this.pageMenuTables = true;
          this.menuController.enable(true, 'second');
          this.menuController.enable(false, 'third');
          this.menuController.enable(false, 'four');
        }else{
          this.pageMenuTables = false;
        }

        if(this.currentPage.slice(1,15) == 'detalle-pedido'){
          this.pageMenuDetail = true;
          this.menuController.enable(false, 'second');
          this.menuController.enable(true, 'third');
          this.menuController.enable(false, 'four');
        }else{
          this.pageMenuDetail = false;
        }

        if(this.currentPage.slice(1,9) == 'delivery'){
          this.pageMenuDelivery = true;
          this.menuController.enable(false, 'second');
          this.menuController.enable(false, 'third');
          this.menuController.enable(true, 'four');
        }else{
          this.pageMenuDelivery = false;
        }
      }
    });
  }

  //-- Init --//
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    //-- Verify Loggin commerce and login employeed --//
    // LO COMENTE A PROPOSITO PARA HACERLO POR EL GUARDS
    // this.verifyLoggin();

    //-- Hide Menu --//
    // this.hideMenu();  

    //-- Update Order --//
    this.socket.fromEvent('update-item-status-order').subscribe(data => {     
      //Hide item box
      let itemBox = document.getElementById('itemOrder'+data['order'].idDetallePedido);
      console.log(itemBox);
      if(itemBox){
        itemBox.style.marginLeft = "100px";
        itemBox.style.opacity  = "0";
        itemBox.style.transition = "all 0.60s";
    
        setTimeout(() => {
          itemBox.style.display = "none";

          // Delete item for array
          const searchOrder = this.ordersReady.find(element => element.numeroTicket == data['order'].numeroTicket);
          const index = this.ordersReady.indexOf(searchOrder);

          if (index !== -1) {
            this.ordersReady.splice(index, 1);
          }    
        },800);
      }
    });    
  }

  //-- Verify Loggin commerce and login employeed --//
  verifyLoggin(){
    this.storage.get('isLoggin').then((resultLoggin) => {
     console.log("Logeado",resultLoggin) ;
     if(resultLoggin){    
       this.storage.get('isLogginPin').then((resultLogginPin) => {
        console.log("Logeado Pin",resultLogginPin) ;
          if(resultLogginPin){    
            // this.navCtrl.navigateRoot('/mesas');
            this.storage.get('nombreMozo').then((resultNameWaiter) => {
              if(resultNameWaiter){   
                this.userName = resultNameWaiter;

                this.storage.get('idUsuarioMozo').then((resultIdUserWaiter) => {
                  if(resultIdUserWaiter){   
                    this.idUserWaiter = resultIdUserWaiter;

                    // Load scripts
                    this.loadCouriers();
                  }
                })                
              }
            })
          }else{
            this.navCtrl.navigateRoot('/login-pin-empleado');
          }
        })
      }else{
        this.navCtrl.navigateRoot('/login-comercio');
      }
    });     
  }

  //-- Random color --//
  getRandomColor() {
    // return `hsla(${~~(360 * Math.random())},70%,70%,0.8)`
    return "hsla(" + ~~(360 * Math.random()) + "," +
                    "70%,"+
                    "80%,1)"
  }
  
  //-- Generate array validators form detail order --//
  get getProductsForm(){
    return this.detailForm.get('products') as FormArray;
  }

  //-- Open box new courier --//
  openNewCourier(focus){
    this.checkNewCourier = !this.checkNewCourier;
    
    if(focus){
      setTimeout(() => {
        this.inputNameCourierFocus.setFocus();
      },100);    
    }
  }

  //-- Load couriers --//
  loadCouriers(){
    this.deliveryService.couriers().then((result) => {
      console.log('aca');
      let data:any = result;
      console.log(data);
      
      if(data.valid){
        // this.alertService.presentToast(data.msg, 'success', 'bottom');
        this.couriers = data.datos;
        this.totalCouriers = this.couriers.length;
      }else{      
        // this.alertService.presentToast(data.msg, 'danger', 'bottom');
        console.log(data.msg);
      } 
    }, (err) => {
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    }); 
  }

  //-- Submit form add courier --//
  submitCourierForm(){
    if(this.detailFormCourier.controls['name'].valid){
      this.deliveryService.setCourier(this.detailFormCourier).then((response) => {
        if(response['valid']){
          // Show message
          this.alertService.presentToast(response['msg'], 'success', 'bottom');    
          this.checkNewCourier = !this.checkNewCourier;    
          this.detailFormCourier.reset();
          this.loadCouriers();
        }else{
          // Show message
          this.alertService.presentToast(response['msg'], 'danger', 'bottom');
        }
      })
      .catch((reject) => {
        (console.log(reject))
        this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
      });
    }
  }  
  
  //-- Popover tables user waiter --//
  async showPopOrders(event,dataCourier) {
    const popover = await this.popoverCtrl.create({
      component: OrdersCourierPage,
      event: event,
      componentProps: {courier: dataCourier},
      mode: 'ios',
      backdropDismiss: true,
      cssClass: 'pop-over-orders-courier'
    });

    await popover.present();
  }  
 
  //-- Update Order --//
  updateStatusOrder(item){
    this.ordersService.updateStatusOrder(item, 3).then((response) => {
      if(response['valid']){
        // Update status order
        this.socket.emit('update-status-order', { order: item });        

        // Show message
        this.alertService.presentToast(response['msg'], 'success', 'bottom');        
      }else{
        // Show message
        this.alertService.presentToast(response['msg'], 'danger', 'bottom');
      }
    })
    .catch((reject) => {
      (console.log(reject))
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });
  }

  //-- Submit form detail order --//
  submitDetailForm(){
    console.log("Controls",<FormArray>this.detailForm.controls['products']);
  }

  //-- Open and close item detail --//
  toggleSection(i) {
    // Formarray detail order
    const control = <FormArray>this.detailForm.controls['products'];
    const openItem = control.value[i]['openItem'];
    control.value[i]['openItem'] = !openItem;
    this.detailForm.controls['products'].setValue(control.value);
  }  

  //-- Open and close commnet box --//
  toggleSectionComment(i) {
    // Formarray detail order
    const control = <FormArray>this.detailForm.controls['products'];
    const openComment = control.value[i]['openComment'];
    control.value[i]['openComment'] = !openComment;
    this.detailForm.controls['products'].setValue(control.value);

    setTimeout(() => {
      const inputFieldIDElement = document.getElementById('inputComment' + i);
      if (inputFieldIDElement) {
        const inputTagCollection = inputFieldIDElement.getElementsByTagName("input");
        if (inputTagCollection && inputTagCollection.item(0)) {
          const inputElement = inputTagCollection.item(0);
          inputElement.focus();
        }
      }
    }, 300);    
  }  

  //-- Increment and decrement quantity product --//
  increment(index){
    const control = <FormArray>this.detailForm.controls['products'];
    let newCantidad =control.value[index]['cantidad'] + 1;
    if(newCantidad <= 1){
      control.value[index]['cantidad'] = 1;
    }else{
      control.value[index]['cantidad'] = newCantidad;
    }
    control.value[index]['subtotal'] = control.value[index]['cantidad']*control.value[index]['precio'];
    this.detailForm.controls['products'].setValue(control.value);
    
    this.subTotal += Number(control.value[index]['precio']);
    this.total += Number(control.value[index]['precio']);
  }
  
  decrement(index){
    const control = <FormArray>this.detailForm.controls['products'];
    let newCantidad =control.value[index]['cantidad'] - 1;
    console.log(newCantidad);
    if(newCantidad < 1){
      control.value[index]['cantidad'] = 1;
      //I subtract the price of th item to the subtotal
      // this.subTotal -= Number(control.value[index]['precio']);
    }else if(newCantidad >= 1){
      control.value[index]['cantidad'] = newCantidad;
      //I subtract the price of th item to the subtotal
      this.subTotal -= Number(control.value[index]['precio']);
      this.total -= Number(control.value[index]['precio']);
    }
    control.value[index]['subtotal'] = control.value[index]['cantidad']*control.value[index]['precio'];
    this.detailForm.controls['products'].setValue(control.value);
  }
  
  //-- Delete input form detail order --//
  deleteItem(index: number){
    const control = <FormArray>this.detailForm.controls['products'];
    this.subTotal -=  control.value[index]['cantidad']*control.value[index]['precio'];
    this.total -=  control.value[index]['cantidad']*control.value[index]['precio'];
    control.removeAt(index);    
    if (control.value.length > 0) {
      this.ticketValid = true;
    } else {
      this.ticketValid = false;
    }
  }

  //-- Pay order --//
  submitPayOrder(){
    var dataOrder={
      'idGenPedido':this.idGenPedido,
      'idCliente':1,
      'tipoFac':6,
      'idMozo':36,
      'idEstado':1,
      'idTab':1
    };
    
    const control = <FormArray>this.detailForm.controls['products'];
    this.ordersService.setPayOrder(dataOrder).then((response) => {
      if(response['valid']){
        // Update tables waiter
        this.socket.emit('update-table-user-online', { idUserWaiter: this.idUserWaiter, nameWaiter: this.userName, table: this.table, tableOld: false, updateTable: 0 });

        // Update tables dashboard
        this.table['nombreColor'] = 'no-gradient';
        this.table['nombreColorBtn'] = 'gradient-btn';
        this.table['idPedido'] = 0;
        this.table['idEstadoMesa'] = 1;
        this.table['pedidos'] = false;  

        this.socket.emit('update-open-table', { updateTable: this.table });        

        // Redirect 
        this.router.navigate(['/mesas']);

        // Show message
        this.alertService.presentToast(response['msg'], 'success', 'bottom');        
      }else{
        // Show message
        this.alertService.presentToast(response['msg'], 'danger', 'bottom');
      }
    })
    .catch((reject) => {
      (console.log(reject))
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });
  }

  //-- Pay order bar --//
  submitPayOrderBar(){
    this.idGenPedido;
    
    // Sum total detail order
    let total = 0;
    this.subTotalOrder = 0;
    <FormArray>this.detailForm.controls['products'].value.forEach(element => {
      total = Number(element.precio) * Number(element.cantidad);
      this.subTotalOrder = Number(total) + Number(this.subTotalOrder);
    })

    const control = <FormArray>this.detailForm.controls['products'];
    this.ordersService.setOrder(this.idGenPedido,control.value).then((response) => {

      if(response['valid']){
        // Show message
        this.alertService.presentToast(response['msg'], 'success', 'bottom');

        // Set PayOrderBar
        var dataOrder={
          'idGenPedido':this.idGenPedido,
          'idCliente':1,
          'tipoFac':6,
          'idMozo':36,
          'idEstado':1,
          'idTab':2,
        };
        
        const control = <FormArray>this.detailForm.controls['products'];
        this.ordersService.setPayOrder(dataOrder).then((response) => {
          if(response['valid']){
            // Redirect 
            this.router.navigate(['/mostrador']);
    
            // Show message
            this.alertService.presentToast(response['msg'], 'success', 'bottom');        
          }else{
            // Show message
            this.alertService.presentToast(response['msg'], 'danger', 'bottom');
          }
        })
        .catch((reject) => {
          (console.log(reject))
          this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
        });
      }else{
        // Show message
        this.alertService.presentToast(response['msg'], 'danger', 'bottom');
      }
    })
    .catch((reject) => {
      (console.log(reject))
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });
  }

  //-- Submit form detail order --//
  submitTicketForm(){
    this.idGenPedido;
    
    // Sum total detail order
    let total = 0;
    this.subTotalOrder = 0;
    <FormArray>this.detailForm.controls['products'].value.forEach(element => {
      total = Number(element.precio) * Number(element.cantidad);
      this.subTotalOrder = Number(total) + Number(this.subTotalOrder);
    })

    const control = <FormArray>this.detailForm.controls['products'];
    this.ordersService.setOrder(this.idGenPedido,control.value).then((response) => {      
      if(response['valid']){
        // Update tables dashboard
        this.table['nombreColor'] = 'gradient';
        this.table['nombreColorBtn'] = 'no-gradient-btn';        
        this.table['pedidos'] = [{
          nombre: this.userName,
          totalDetalle: this.subTotalOrder
        }];     

        this.socket.emit('update-open-table', { updateTable: this.table });      

        // Redirect 
        this.router.navigate(['/mesas']);

        // Show message
        this.alertService.presentToast(response['msg'], 'success', 'bottom');
      }else{
        // Show message
        this.alertService.presentToast(response['msg'], 'danger', 'bottom');
      }
    })
    .catch((reject) => {
      (console.log(reject))
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });
  }

  //-- Hide Menu --//
  hideMenu(){
    this.menuController.isEnabled().then((resultEnabled)=>{
      this.menuController.open().then((resultOpen)=>{
        if(!resultOpen){
          if(!resultEnabled){
            this.menuController.enable(false);
          }
        }
      })
    })
  }      

  //-- Keyboard shortcuts --//
  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    var key = (event.keyCode || event.which);

    //Save state keypress
    if(this.stateKeypress.indexOf(key) == -1){
      this.stateKeypress.push(key);
    }

    //Verify shortcut three keypress
    if(this.stateKeypress.length == 3){
      if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(72)){
        //Goto home - Ctrl + Alt + H 
        this.navCtrl.navigateRoot('/home');
      }else if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(73)){
        //Goto login commerce - Ctrl + Alt + I 
        this.navCtrl.navigateRoot('/login-comercio');
      }else if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(77)){
        //Goto login pin employeed - Ctrl + Alt + M 
        this.navCtrl.navigateRoot('/login-pin-empleado');
      }else if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(68)){
        //Goto delivery - Ctrl + Alt + D
        this.navCtrl.navigateRoot('/delivery');
      }else if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(67)){
        //Close session commerce - Ctrl + Alt + C 
        this.logout();
      }else if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(77)){
        //Close session waiter - Ctrl + Alt + M 
        this.logoutWaiter();
      }

    //Clean
    this.cleanStateKeypress();

    //Verify shortcut four keypress
    }else if(this.stateKeypress.length == 4){
      // console.log("Four ", this.stateKeypress.length, this.stateKeypress);

      if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(77) && this.stateKeypress.includes(69)){
        //Goto tables - Ctrl + Alt + M + E
        this.navCtrl.navigateRoot('/mesas');
      }else if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(77) && this.stateKeypress.includes(79)){
        //Goto counter - Ctrl + Alt + M + O
        this.navCtrl.navigateRoot('/mostrador');
      }else if(this.stateKeypress.includes(18) && this.stateKeypress.includes(17) && this.stateKeypress.includes(77) && this.stateKeypress.includes(67)){
        //Goto change waiter - Ctrl + Alt + M + C
        this.logoutWaiter();
        this.navCtrl.navigateRoot('/login-pin-empleado');
      }      

      //Clean
      this.cleanStateKeypress();
    }
  }   

  //-- Clean state keypress --//
  cleanStateKeypress(){
    setTimeout(() => {
      this.stateKeypress = [];
    },200)  
  }

  //-- Logout - Commerce --//
  logout(){
    // Set storage false
    this.isLoggin = false;     
    this.isLogginPin = false;   
    this.isLogginKitchen = false;   
    this.storage.clear();
    this.navCtrl.navigateRoot('/login-comercio');
  }

  //-- Logout - Waiter --//
  logoutWaiter(){
    // Set status disconnect
    this.storage.get('idUsuarioMozo').then((val) => {
      if(val){
        this.authService.logout_waiter(val).then((result) => {
          let data:any = result;

          if(data.valid){
            // Delete waiter online
            this.socket.emit('del-user-online', { idUserWaiter: val });

            // Set storage false
            this.isLogginPin = false;     
            this.storage.set('isLogginPin', false);
            this.storage.set('nombreMozo', false);
            this.storage.set('apellidoMozo', false);
            this.storage.set('idUsuarioMozo', false);
            this.navCtrl.navigateRoot('/login-pin-empleado');
          }else{      
            this.alertService.presentToast(data.msg, 'danger', 'bottom');
          } 
        }, (err) => {
          this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
        });  
      }else{
        this.navCtrl.navigateRoot('/login-pin-empleado');
        this.alertService.presentToast('Error al cambair estado del mozo', 'danger', 'bottom');
      }
    })
  }
}
