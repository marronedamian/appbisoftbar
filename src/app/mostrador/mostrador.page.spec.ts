import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MostradorPage } from './mostrador.page';

describe('MostradorPage', () => {
  let component: MostradorPage;
  let fixture: ComponentFixture<MostradorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostradorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MostradorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
