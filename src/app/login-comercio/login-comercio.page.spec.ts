import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginComercioPage } from './login-comercio.page';

describe('LoginComercioPage', () => {
  let component: LoginComercioPage;
  let fixture: ComponentFixture<LoginComercioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComercioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComercioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
