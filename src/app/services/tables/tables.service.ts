import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { EnvService } from '../env/env.service';

declare var require: any

@Injectable({
  providedIn: 'root'
})
export class TablesService {

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
  ) { }

  tables() {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_mesas/listar_mesas',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      }
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice listado mesas. Cod Err #2000"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }  

  tablesAvailables() {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_mesas/busca_mesas_libres',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      }
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice listado mesas. Cod Err #2000"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }  

  changeTable(tableRol,newTable) {
    var request = require("request");
    var options = { 
      method: 'POST',
      url: this.env.API_URL+'api_mesas/cambiar_mesa',
      headers: 
      { 
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      form: 
      {
        idMesa: tableRol.id, 
        idMesaNueva: newTable.id,
        idPedido: tableRol.idPedido,
      } 
    }; 
 
    return new Promise(res => {
      request(options, function (error, response, body) {
        if (error){ 
          let result = JSON.parse('{"valid":false, "msg":"Error de conexión al webservice listado mesas. Cod Err #2000"}');
          res(result);
        }else{
          let result = JSON.parse(body);
          res(result);
        };
      });      
    })
  }  
}
