import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComercioPage } from './login-comercio.page';

const routes: Routes = [
  {
    path: '',
    component: LoginComercioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginComercioPageRoutingModule {}
