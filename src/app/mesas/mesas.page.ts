import { ModalController, NavController, PopoverController, ActionSheetController, MenuController, Events } from '@ionic/angular';
import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { TablesService } from 'src/app/services/tables/tables.service';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { UserService } from 'src/app/services/user/user.service';
import { AppComponent } from '../app.component';
import { Socket } from 'ngx-socket-io';
import { TablesUserPage } from '../popovers/tables-user/tables-user.page';
 
@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.page.html',
  styleUrls: ['./mesas.page.scss'],
})
export class MesasPage implements OnInit {
  nameWaiter: string;
  tabTableSelected: string = 'grid';
  checkedList: boolean = false;
  checkedGrid: boolean = true;
  ordersReady: any[];
  tables: any[];
  tableSearch: any[];
  tablesAvaible: Number = 0;
  btnTablesAvailables: Array<any> = [];
  table: any;
  statusMenu: boolean = true;
  statusMenuTables: boolean = false;
  statusUsersOnline: boolean = false;
  statusError: boolean = false;
  statusSearchError: boolean = false;
  msgError: string;
  usersOnline: Array<any> = [];
  idUserWaiter: Number = 0;
 
  constructor(
    private modalController: ModalController,
    private tablesService: TablesService,
    private ordersService: OrdersService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private userService: UserService,
    private storage: Storage,
    public event: Events,  
    private menuController: MenuController,
    public actionSheetController: ActionSheetController,
    public popoverCtrl: PopoverController,
    public router: Router,
    private socket: Socket, 
  ) { 
    // -- //
  }

  ngOnInit() {
    //-- Info Waiter --//
    this.getInfoWaiter(0);

    //-- Get users online --//
    this.getUsersOnline();

    //-- Transactions Socket --//
    this.socket.connect();
  
    //-- Get users online --//
    this.socket.fromEvent('get-users-online').subscribe(data => {
      const searchUser = this.usersOnline.find( users => users.idUserWaiter === data['idUserWaiter']);
      if(!searchUser){
        // If it does not exist I add it to the list
        this.usersOnline.push(data);
      }
    });
  
    //-- Delete user online --//
    this.socket.fromEvent('del-item-user-online').subscribe(data => {      
      const searchUser = this.usersOnline.find(element => element.idUserWaiter == data['idUserWaiter']);
      const index = this.usersOnline.indexOf(searchUser);

      if (index !== -1) {
        this.usersOnline.splice(index, 1);
      }
    });
  
    //-- Update user online --//
    this.socket.fromEvent('update-item-user-online').subscribe(data => {      
      const searchUser = this.usersOnline.find(element => element.idUserWaiter == data['idUserWaiter']);
      const index = this.usersOnline.indexOf(searchUser);

      let addTableUser = 0;

      // If there is a waiter, I update
      if (index !== -1) {
        // Notes
        // 0 - Remove / 1 - Add / 2 - Change 

        if(data['updateTable'] == 1){
          // Update total tables
          addTableUser = Number(searchUser.tablesOpen) + 1;

          // Add
          this.usersOnline[index]['tablesUser'].push(data['table']);
          this.usersOnline[index]['tablesOpen'] = addTableUser;          
        }else if(data['updateTable'] == 0 && searchUser.tablesOpen > 0){          
          // Search table in waiter
          const searchTableUser = this.usersOnline[index]['tablesUser'].find(element => element.idGenMesa == data['table']['idGenMesa']);
          const indexTable = this.usersOnline[index]['tablesUser'].indexOf(searchTableUser);
          
          // Update array tables waiter
          if (indexTable !== -1) {
            this.usersOnline[index]['tablesUser'].splice(indexTable, 1);
          }

          // Update total tables
          addTableUser = Number(searchUser.tablesOpen) - 1;

          // Remove
          this.usersOnline[index]['tablesOpen'] = addTableUser;          
        }if(data['updateTable'] == 2){
          // Search table in waiter
          const searchTableUser = this.usersOnline[index]['tablesUser'].find(element => element.idGenMesa == data['tableOld']['idGenMesa']);
          const indexTable = this.usersOnline[index]['tablesUser'].indexOf(searchTableUser);

          // Update array tables waiter
          if (indexTable !== -1) {
            this.usersOnline[index]['tablesUser'].splice(indexTable, 1);
            this.usersOnline[index]['tablesUser'].push(data['table']);            
          }   
        }
      }
    });
    
    //-- Update tables dashboard --//
    this.socket.fromEvent('update-item-open-table').subscribe(data => {   
      if(!this.tables){
        this.tables = data['table'];
      }else{
        const searchTable = this.tables.find(element => element.id == data['table'].id);
        const index = this.tables.indexOf(searchTable);

        if (index !== -1) {
          this.tables[index] = data['table'];
        }
      }
    });

    //-- Change tables dashboard --//
    this.socket.fromEvent('change-item-open-table').subscribe(data => {   
      // New Table
      const searchTable = this.tables.find(element => element.id == data['newTable'].id);
      const index = this.tables.indexOf(searchTable);

      if (index !== -1) {
        this.tables[index] = data['newTable'];
      }

      // Old Table
      const searchOldTable = this.tables.find(element => element.id == data['oldTable'].id);
      const indexOld = this.tables.indexOf(searchOldTable);

      if (indexOld !== -1) {
        this.tables[indexOld] = data['oldTable'];
      }
    });
  }
 
  //-- Get info Waiter --//
  getInfoWaiter(typeFilter){
    this.storage.get('idUsuarioMozo').then((val) => {
      //-- Set css segment --//
      if(typeFilter==0){
        this.checkedList = false;
        this.checkedGrid = true;
      }else if(typeFilter==1){
        this.checkedList = true;
        this.checkedGrid = false;
      }

      //-- List tables --//
      if(val){
        this.statusError = false;
        this.idUserWaiter = val;
        this.loadTables();
        this.loadOrdersReady();

        this.storage.get('nombreMozo').then((name) => {
          if(name){
            this.nameWaiter = name;
          }
        })
      }else{
        this.statusError = true;
        this.msgError = 'Error al obtener los datos del mozo';
        this.alertService.presentToast('Error al obtener los datos del mozo', 'danger', 'bottom');
      }
    })
  }

  //-- Get users online --//
  getUsersOnline(){
    this.userService.usersOnline().then((result) => {
      let data:any = result;
      
      if(data.valid){
        this.usersOnline = data.usuarios;
      }else{      
        this.statusError = true;
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.statusError = true;
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });   
  }

  //-- Search tables --//
  getItems(ev: any) {
    // Reset items back to all of the items
    this.tableSearch = this.tables;

    // Set val to the value of the searchbar
    var val = ev.target.value;

    // If the value is an empty string don't filter the items
    if (val.length != 0){
      this.tableSearch = this.tableSearch.filter((item) => {
        return (item.descripcion.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })    

      if(this.tableSearch.length == 0){
        this.statusSearchError = true;
      }else{
        this.statusSearchError = false;
      }
    }
  }  
 
  //-- Show/Hide Menu --//
  showHideMenu(){
    let classNameMenu = document.querySelector('#menuUser').className;
    let classMenu = classNameMenu.substr(93,101);
    let classHideMenu = classNameMenu.substr(0,8);

    if(classMenu == 'showMenu'){
      this.event.publish('statusMenuHide');
      this.statusMenu = false;  
    }else if(classMenu == 'hideMenu'){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }else if (classHideMenu == 'hideMenu' && this.statusMenu ){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }
  }

  //-- Show/Hide Menu Tables --//
  showHideMenuTables(){
    // CSS
    let classNameMenu = document.querySelector('#menuInfoOrders').className;
    let classMenu = classNameMenu.substr(69,14);
    let classShowMenu = classNameMenu.substr(78,14);
    let classHideMenu = classNameMenu.substr(91,14);

    if(classMenu == 'showMenuTables' || classShowMenu == 'showMenuTables' || classHideMenu == 'showMenuTables'){
      this.event.publish('statusMenuTablesHide');
      this.statusMenuTables = false;
    }else if(classHideMenu == 'hideMenuTables' || classShowMenu == 'hideMenuTables'){
      this.event.publish('statusMenuTablesShow');
      this.statusMenuTables = true;
    }else if (classHideMenu == 'hideMenuTables' && this.statusMenuTables ){
      this.event.publish('statusMenuTablesShow');
      this.statusMenuTables = true;
    }

    // Mobile
    this.menuController.open('second').then((resultOpen)=>{
      if(!resultOpen){
        this.menuController.close('second');
      }else{
        this.menuController.open('second');
      }
    })    
  }  

  //-- Show/Hide User Online --//
  showHideUsersOnline(){
    // CSS
    if(this.statusUsersOnline){
      this.statusUsersOnline = false;
    }else{
      this.statusUsersOnline = true;
    }
  }  

  //-- Filter tables - List or Grid --//
  segmentChanged(event) {
    this.tabTableSelected = event.detail.value;
  }

  //-- Load Orders Ready --//
  loadOrdersReady(){
    this.ordersService.orders(1,null,7).then((result) => {
      let data:any = result;

      if(data.valid){
        //-- Return orders ready form counter --//
        this.event.publish('loadOrdersReady', data.pedidos); 
      }else{      
        //-- Return orders ready form counter --//
        this.event.publish('loadOrdersReady', null); 
      } 
    }, (err) => {
      this.statusError = true;
      this.alertService.presentToast("Ocurrio un error al listar los pedidos listos", 'danger', 'bottom');
    });      
  }
  
  //-- Load Tables --//
  loadTables(){
    this.tablesService.tables().then((result) => {
      let data:any = result;
       
      if(data.valid){
        this.statusError = false;
        this.alertService.presentToast('Mesas listadas', 'success', 'bottom');
        this.tables = data.datos;
        this.tableSearch = data.datos;

        // Count tables avaible
        this.tables.forEach(element => {
            if(element.idEstadoMesa!=2){
              this.tablesAvaible = Number(this.tablesAvaible) + 1;
            }
        });       
        this.event.publish('totalTablesAvaible', this.tablesAvaible);  
      }else{      
        this.statusError = true;
        this.msgError = data.msg;
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.statusError = true;
      this.msgError = 'Ocurrio un error al listar las mesas';
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });      
  }  

  //-- Load detail order --//
  viewDetailOrder(table){
    this.ordersService.detail(1,table.id,this.idUserWaiter).then((result) => {
      let data:any = result;

      if(data.valid){
          if(data.mesa[0].idMozo == this.idUserWaiter){
            this.alertService.presentToast(data.msg, 'primary', 'bottom');

            if(data.nuevo){
              table.idPedido = data.idGenPedido;
              table.idEstadoMesa = 2;
              table.nombreColor = 'gradient';
              table.nombreColorBtn = 'no-gradient-btn';     

              table.pedidos = [{
                nombre: this.nameWaiter,
                totalDetalle: 0
              }];           
              
              // Update tables waiter
              this.socket.emit('update-table-user-online', { idUserWaiter: this.idUserWaiter, nameWaiter: this.nameWaiter, table: table, tableOld: false, updateTable: 1 });
              
              // Update tables dashboard
              this.socket.emit('update-open-table', { updateTable: table });
            }
            
            this.router.navigate(['/detalle-pedido', table.idPedido]);
          }else{
            this.alertService.presentToast("La mesa se encuentra abierta por otro mozo", 'danger', 'bottom');
          }
      }else{      
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    }); 
  }

  //-- Load tables availables --//
  async loadTablesAvailables(tableRol) {
    //-- Search tables --//
    this.tablesService.tablesAvailables().then((result) => {
      let data:any = result;
      
      if(data.valid){
          this.btnTablesAvailables = [];

          data.datos.forEach(table => {
            this.btnTablesAvailables.push({
              text: table.descripcion,
              cssClass: 'menuTablesChange',
              handler: () => {
                this.tableChangedSelected(tableRol,table);
              }
            });
          });
          
          this.menuTablesAvaibles(this.btnTablesAvailables); 
      }else{      
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });       
  }

  //-- Chenged Tables --//
  tableChangedSelected(tableRol,newTable){
    this.tablesService.changeTable(tableRol,newTable).then((result) => {
      let data:any = result;
      
      if(data.valid){        
        // Update new table
        const searchTable = this.tables.find( table => table.id === newTable.id );
        if(searchTable){
          searchTable.nombreColor = 'gradient';
          searchTable.nombreColorBtn = 'no-gradient-btn';
          searchTable.idPedido = tableRol.idPedido;
          searchTable.idEstadoMesa = 2;
          searchTable.pedidos = [{
            nombre: tableRol['pedidos'][0].nombre,
            totalDetalle: tableRol['pedidos'][0].totalDetalle
          }];             
        }

        // Update old table
        tableRol.nombreColor = 'no-gradient';
        tableRol.nombreColorBtn = 'gradient-btn';
        tableRol.idPedido = 0;
        tableRol.idEstadoMesa = 1;
        tableRol.pedidos = false;    
        
        // Update tables waiter
        this.socket.emit('update-table-user-online', { idUserWaiter: this.idUserWaiter, nameWaiter: this.nameWaiter, table: searchTable, tableOld: tableRol, updateTable: 2 });        

        // Change tables dashboard
        this.socket.emit('change-open-table', { newTable: searchTable, oldTable: tableRol });
      }else{      
        this.alertService.presentToast(data.msg, 'danger', 'bottom');
      } 
    }, (err) => {
      this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
    });   
  }

  //-- Set menu actionsheet --//
  async menuTablesAvaibles(json){
    const alert = await this.actionSheetController.create({
      header: 'Mesas disponibles',
      cssClass: 'headMenuTablesChange',
      // subHeader: 'Seleccione',
      buttons: json
    });        
    await alert.present(); 
  }

  //-- Popover tables user waiter --//
  async showPop(event,dataWaiter,tablesOpen) {
    const popover = await this.popoverCtrl.create({
      component: TablesUserPage,
      event: event,
      componentProps: {waiter: dataWaiter, tables: tablesOpen},
      mode: 'ios',
      backdropDismiss: true
    });

    await popover.present();
  }
}
