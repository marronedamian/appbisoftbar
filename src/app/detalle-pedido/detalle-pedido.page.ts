import { ModalController, NavController, ActionSheetController, MenuController, Events } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert/alert.service';

@Component({
  selector: 'app-detalle-pedido',
  templateUrl: './detalle-pedido.page.html',
  styleUrls: ['./detalle-pedido.page.scss'],
})
export class DetallePedidoPage implements OnInit {
  statusMenu: boolean = true;
  statusMenuDetail: boolean = false;  
  titleCategory: boolean = true;  
  categories: any[];
  nameTable: string;
  items: any[];
  itemsList: any[];
  itemsTop: any[];
  category: any;
  statusSearchProducts: boolean = false;
  btnSubProductAvailables: Array<any> = [];
  idGenPedido: any;
  hideViewTop: boolean = true;
  showViewTop: boolean = false;

  constructor(
    private route: ActivatedRoute, 
    public event: Events,
    private alertService: AlertService,
    private menuController: MenuController,
    public actionSheetController: ActionSheetController,
  ) {  
    //-- Get idGenPedido --//
    this.idGenPedido = this.route.snapshot.paramMap.get('idPedido');  

    //-- Return detail order and categories --//
    this.event.publish('loadDetailOrder', this.idGenPedido); 

    //-- Set array categories and products --//
    event.subscribe('setCategoriesProducts', (nameTable, categories, itemsTop ,items) => {
      this.nameTable = nameTable;
      this.categories = categories;
      this.itemsTop =  itemsTop;
      this.items = items;

      if(this.categories.length > 0 || this.categories){
        this.getItems('',1);
      }      
    })
  }

  //-- Changed status btn products top --//
  statusBtnViewTop(status){
    if(status == 1){
      this.showViewTop = true;
      this.hideViewTop = false;
    }else if(status == 0){
      this.showViewTop = false;
      this.hideViewTop = true;
    }
  }

  //-- Show/Hide Menu --//
  showHideMenu(){
    let classNameMenu = document.querySelector('#menuUser').className;
    let classMenu = classNameMenu.substr(93,101);
    let classHideMenu = classNameMenu.substr(0,8);

    if(classMenu == 'showMenu'){
      this.event.publish('statusMenuHide');
      this.statusMenu = false;  
    }else if(classMenu == 'hideMenu'){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }else if (classHideMenu == 'hideMenu' && this.statusMenu ){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }
  }

  //-- Show/Hide Menu Detail --//
  showHideMenuDetail(){
    // CSS
    let classNameMenu = document.querySelector('#menuDetail').className;
    let classMenu = classNameMenu.substr(78,14);
    let classHideMenu = classNameMenu.substr(91,14);

    if(classMenu == 'showMenuDetail' || classHideMenu == 'showMenuDetail'){
      this.event.publish('statusMenuDetailHide');
      this.statusMenuDetail = true;  
    }else if(classMenu == 'hideMenuDetail' || classHideMenu == 'hideMenuDetail'){
      this.event.publish('statusMenuDetailShow');
      this.statusMenuDetail = false;  
    }

    // Browser
    this.menuController.open('third').then((resultOpen)=>{
      if(!resultOpen){
        this.menuController.close('third');
      }else{
        this.menuController.open('third');
      }
    })    
  }  

  //-- Init --//
  ngOnInit() {
  }

  //-- Add product in order list --//
  addItem(item,idTipoProducto){
    this.event.publish('addItemDetailOrder', item, idTipoProducto);
  }  

  //-- Search products --//
  getItems(ev: any, tipo: number) {
    // Reset items back to all of the items
    this.itemsList = this.items;

    // Set val to the value of the searchbar
    if(tipo==0){
      var val = ev.target.value;
    }else{
      var val = ev;
    }

    // Set category name in section all
    if(val==''){
      this.titleCategory = true;
    }else{
      this.titleCategory = false;
    }

    // If the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.itemsList = this.itemsList.filter((item) => {
        return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1) || (item.codigo == val) || (item.idCategoriaProducto == val);
      })    

      if(this.itemsList.length == 0){
        this.statusSearchProducts = true;
      }else{
        this.statusSearchProducts = false;
      }
    }else{
      this.statusSearchProducts = false;
    }
  }  

  //-- Set sub products menu actionsheet --//
  async menuSubProducts(item){
    // Verify prod or sub prod
    if(item.subProductos){
      this.btnSubProductAvailables = [];

      item.subProductos.forEach(product => {
        this.btnSubProductAvailables.push({
          text: product.nombre,
          cssClass: 'menuSubProducts',
          handler: () => {
            // Add sub product in order list
            this.addItem(product,item.idTipoProducto);
            return false;
          }
        });
        this.btnSubProductAvailables.push({
          text: '$ ' + product.precioVenta,
          cssClass: 'menuPriceSubProducts',
          handler: () => {
            // Add sub product in order list
            this.addItem(product,item.idTipoProducto);
            return false;
          }
        });
      });
 
      const alert = await this.actionSheetController.create({
        header: 'Seleccione',
        cssClass: 'headMenuSubProducts',
        // subHeader: 'Seleccione',
        buttons: this.btnSubProductAvailables
      });        
      await alert.present(); 
    }else{
      // Add product in order list
      this.addItem(item,item.idTipoProducto);
    }
  }
}
