import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CocinaPage } from './cocina.page';

describe('CocinaPage', () => {
  let component: CocinaPage;
  let fixture: ComponentFixture<CocinaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CocinaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CocinaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
