import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MostradorPageRoutingModule } from './mostrador-routing.module';
import { MostradorPage } from './mostrador.page';
import { TimeAgoComponent } from '../pipes/time-ago/time-ago.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MostradorPageRoutingModule,
    TimeAgoComponent
  ],
  declarations: [MostradorPage],
})
export class MostradorPageModule {}
