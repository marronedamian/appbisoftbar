import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Storage } from '@ionic/storage';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login-comercio.page.html',
  styleUrls: ['./login-comercio.page.scss'],
})
export class LoginComercioPage implements OnInit {
  @ViewChild('user', {static: false}) inputUserFocus;
  @ViewChild('pin', {static: false}) inputPinFocus;  
  @ViewChild('form',  {static: false}) formLoginCommerce: NgForm;
  
  statusMenu: boolean = true;
  textLogin: String = 'Toma de pedidos';
  sectionActive: any = 2;

  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private storage: Storage,
    public event: Events,
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.inputUserFocus.setFocus();
    },2500)    
  }  

  //-- KeyUp event --//
  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    var key = (event.keyCode || event.which);

    //Loggin keypress enter
    if(key == 13){
      this.formLoginCommerce.ngSubmit.emit();
    }
  }  

  //-- Chenge Section --//
  changeSection(section){
    if(section == 1){
      // this.textLogin = 'Administración';
      // this.sectionActive = 1;
    }else if(section == 2){
      this.textLogin = 'Toma de pedidos';
      this.sectionActive = 2;

      // Focus in input
      setTimeout(() => {
        this.inputUserFocus.setFocus();
      },200)        
    }else if(section == 3){
      this.textLogin = 'Cocina';
      this.sectionActive = 3;

      // Focus in input
      setTimeout(() => {
        this.inputPinFocus.setFocus();
      },200)           
    }
  }

  //-- Dismiss Login Modal --//
  dismissLogin() {
    this.modalController.dismiss();
  }

  //-- Show/Hide Menu --//
  showHideMenu(){
    let classNameMenu = document.querySelector('#menuUser').className;
    let classMenu = classNameMenu.substr(93,101);
    let classHideMenu = classNameMenu.substr(0,8);

    if(classMenu == 'showMenu'){
      this.event.publish('statusMenuHide');
      this.statusMenu = false;  
    }else if(classMenu == 'hideMenu'){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }else if (classHideMenu == 'hideMenu' && this.statusMenu ){
      this.event.publish('statusMenuShow');
      this.statusMenu = true;  
    }
  }

  //-- Loggin User and Pass - Commerce --//
  login(form: NgForm,sectionActive) {
    switch (sectionActive) {
      case 1:
        // Admin
        break;
      case 2:
        // Get orders
        this.authService.login(form.value.user, form.value.password).then((result) => {
          let data:any = result;
     
          if(data.valid){
              this.alertService.presentToast('Usuario logeado con &eacute;xito', 'success', 'bottom');
    
              this.storage.set('isLoggin', data.valid);
              this.storage.set('idGenUsuario', data.datos[0].idGenUsuario);
              this.storage.set('usuario', data.datos[0].usuario);
              this.storage.set('nombreUsuario', data.datos[0].nombre);          
              this.storage.set('idNivel', data.datos[0].idNivel);          
              this.storage.set('idGenComercio', data.datos[0].idGenUsuario);

              this.navCtrl.navigateRoot('/login-pin-empleado');

              this.event.publish('loggedIn');
          }else{      
            this.alertService.presentToast(data.msg, 'danger', 'bottom');
          } 
        }, (err) => {
          form.reset();
          this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
        });        
        break;
      case 3:
        // Kitchen
        this.authService.login_pin_cocina(form.value.pin).then((result) => {
          let data:any = result;
     
          if(data.valid){
              this.alertService.presentToast('Usuario logeado con &eacute;xito', 'success', 'bottom');
    
              this.storage.set('isLoggin', data.valid);
              this.storage.set('isLogginKitchen', true);
              this.storage.set('idGenUsuario', data.datos[0].idGenUsuario);
              this.storage.set('usuario', data.datos[0].usuario);
              this.storage.set('nombreUsuario', data.datos[0].nombre);          
              this.storage.set('idNivel', data.datos[0].idNivel);          
              this.storage.set('idGenComercio', data.datos[0].idGenUsuario);
        
              this.navCtrl.navigateRoot('/cocina');
            
              this.event.publish('loggedIn');
              this.event.publish('loggedInKitchen');
          }else{      
            this.alertService.presentToast(data.msg, 'danger', 'bottom');
          } 
        }, (err) => {
          form.reset();
          this.alertService.presentToast("Ha ocurrido un error", 'danger', 'bottom');
        });             
        break;
    
      default:
        break;
    }
  }   
}
