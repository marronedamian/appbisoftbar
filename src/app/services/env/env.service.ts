import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
 
export class EnvService {
  // ionic serve --address=0.0.0.0 --port=8100 -- --disable-host-check

  API_URL = 'http://localhost/bisoftbarback/';
  // API_URL = 'http://192.168.0.8/bisoftbarback/';
  // API_URL = 'http://test.bisoft.com.ar/';
  // API_URL = 'http://tsoftpc.ddns.net/bisoftbarback/';

  constructor() { }
}