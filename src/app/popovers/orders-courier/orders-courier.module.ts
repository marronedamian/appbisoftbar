import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { OrdersCourierPageRoutingModule } from './orders-courier-routing.module';
import { OrdersCourierPage } from './orders-courier.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersCourierPageRoutingModule,
    IonicSelectableModule,
  ],
  declarations: [OrdersCourierPage]
})
export class OrdersCourierPageModule {}
