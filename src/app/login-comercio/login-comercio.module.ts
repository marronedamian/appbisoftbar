import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginComercioPageRoutingModule } from './login-comercio-routing.module';

import { LoginComercioPage } from './login-comercio.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginComercioPageRoutingModule
  ],
  declarations: [LoginComercioPage]
})
export class LoginComercioPageModule {}
