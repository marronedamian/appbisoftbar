import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../app/guards/auth.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login-comercio',
    pathMatch: 'full'
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login-comercio',
    loadChildren: () => import('./login-comercio/login-comercio.module').then( m => m.LoginComercioPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"login-comercio",
      section:"login"
    }
  },
  {
    path: 'login-pin-empleado',
    loadChildren: () => import('./login-pin-empleado/login-pin-empleado.module').then( m => m.LoginPinEmpleadoPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"login-pin-empleado",
      section:"login"
    }
  },
  {
    path: 'mesas',
    loadChildren: () => import('./mesas/mesas.module').then( m => m.MesasPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"mesas",
      section:"bar"
    }
  },
  {
    path: 'detalle-pedido/:idPedido',
    loadChildren: () => import('./detalle-pedido/detalle-pedido.module').then( m => m.DetallePedidoPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"detalle-pedido",
      section:"bar"
    }
  },
  {
    path: 'delivery',
    loadChildren: () => import('./delivery/delivery.module').then( m => m.DeliveryPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"delivery",
      section:"bar"
    }
  },
  {
    path: 'mostrador',
    loadChildren: () => import('./mostrador/mostrador.module').then( m => m.MostradorPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"mostrador",
      section:"bar"
    }
  },
  {
    path: 'notificaciones',
    loadChildren: () => import('./notificaciones/notificaciones.module').then( m => m.NotificacionesPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"notificaciones",
      section:"bar"
    }
  },
  {
    path: 'perfil',
    loadChildren: () => import('./perfil/perfil.module').then( m => m.PerfilPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"perfil",
      section:"bar"
    }
  }, 
  {
    path: 'cocina',
    loadChildren: () => import('./pages/cocina/cocina.module').then( m => m.CocinaPageModule),
    canActivate: [AuthGuard],
    data:{
      view:"cocina",
      section:"cocina"
    }
  },
  {
    path: 'orders-courier',
    loadChildren: () => import('./popovers/orders-courier/orders-courier.module').then( m => m.OrdersCourierPageModule)
  },
  {
    path: 'tables-user',
    loadChildren: () => import('./popovers/tables-user/tables-user.module').then( m => m.TablesUserPageModule)
  },
  {
    path: 'couriers',
    loadChildren: () => import('./popovers/couriers/couriers.module').then( m => m.CouriersPageModule)
  },
  { 
    path: '**', 
    redirectTo: "" 
  },
];


// { path: '**', component: PageNotFoundComponent }
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
