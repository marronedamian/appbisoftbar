import { Component, OnInit, NgModule } from '@angular/core';
import { TimeAgoPipe } from 'time-ago-pipe';

@NgModule({
  declarations:[TimeAgoPipe],
  exports:[TimeAgoPipe]
})

@Component({
  selector: 'app-time-ago',
  templateUrl: './time-ago.component.html',
  styleUrls: ['./time-ago.component.scss'],
})

export class TimeAgoComponent implements OnInit {
  
  constructor() { }
  
  ngOnInit() {}
  
}
